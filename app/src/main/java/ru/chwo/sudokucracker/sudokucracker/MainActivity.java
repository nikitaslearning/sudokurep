package ru.chwo.sudokucracker.sudokucracker;

import android.annotation.SuppressLint;
import android.graphics.*;
import android.hardware.Camera;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.googlecode.tesseract.android.TessBaseAPI;

import java.util.HashMap;

import static android.view.View.*;


public class MainActivity extends AppCompatActivity {

    Camera camera;
    FrameLayout frameLayout;
    ShowCamera showCamera;
    TextView textView;
    String s = "!";
    Bitmap bitmap;
    ImageView imageView;
    Bitmap bmMonitor;
    Button colorBut;
    Button sudokuBut;
    ImageView smokedView;
    ImageView capture;
    View view;


    public static int alpha = 77;
    public static int red = 77;
    public static int green = 77;
    public static int blue = 77;

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frameLayout = findViewById(R.id.myframe);
        colorBut = findViewById(R.id.colorBut);
        sudokuBut = findViewById(R.id.sudokuBut);
        textView = findViewById(R.id.textView);
        capture = findViewById(R.id.capture);
        smokedView = findViewById(R.id.smokedView);
        view = findViewById(R.id.view);
        camera = Camera.open();
        showCamera = new ShowCamera(this, camera);
        frameLayout.addView(showCamera);

        textView.setText(s);



        OnClickListener onClickListenerColor = new OnClickListener() {
            @Override
            public void onClick(View v) {
                colorBut.setVisibility(INVISIBLE);
                sudokuBut.setVisibility(INVISIBLE);
                capture.setVisibility(VISIBLE);
                switch (v.getId()) {
                    case R.id.colorBut:
                        bmMonitor = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
                        imageView = findViewById(R.id.image);
                        imageView.setImageBitmap(bmMonitor);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    while (true) {
                                        Thread.sleep(300);
                                        camera.takePicture(null, null, null, new Camera.PictureCallback() {
                                            @Override
                                            public void onPictureTaken(byte[] data, Camera camera) {
                                                bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                                                s = getColor(bitmap);
                                            }
                                        });
                                        textView.setText(s);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    while (true) {
                                        Thread.sleep(300);
                                        bmMonitor = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
                                        Canvas canvas = new Canvas(bmMonitor);
                                        Paint paint = new Paint();
                                        paint.setARGB(alpha, red, green, blue);
                                        canvas.drawCircle(50, 50, 30, paint);
                                        imageView.setImageBitmap(bmMonitor);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                        break;
                    case R.id.sudokuBut:

                        smokedView.setVisibility(VISIBLE);
                        Canvas canvas = new Canvas();
                        view.draw(canvas);


//                        myframe.getDrawingRect(new Rect());
                        Button photoBtn = findViewById(R.id.photobutton);
                        photoBtn.setVisibility(VISIBLE);
                        OnClickListener onClickListenerSudoku = new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                camera.takePicture(null, null, null, new Camera.PictureCallback() {
                                    @Override
                                    public void onPictureTaken(byte[] data, Camera camera) {

                                        TessBaseAPI tessBaseAPI = new TessBaseAPI();
                                        String datapath = Environment.getExternalStorageDirectory() + "/ssocr/";
                                        String language = "eng";
                                        tessBaseAPI.setDebug(true);

                                        tessBaseAPI.init(datapath, language);
//                                        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_OSD_ONLY);
//                                        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_LINE);
//                                        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_CHAR);
                                        System.out.println("VariableVariableVariableVariableVariableVariableVariable111111111111111111111111111");
                                        System.out.println(tessBaseAPI.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, ".,!?@#$%&*()<>_-+=/:;'\"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "));
                                        System.out.println("VariableVariableVariableVariableVariableVariableVariable222222222222222222222222222");
                                        System.out.println(tessBaseAPI.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, "0123456789"));
//                                        System.out.println("VariableVariableVariableVariableVariableVariableVariable333333333333333333333333333");
//                                        System.out.println(tessBaseAPI.setVariable("classify_bln_numeric_mode", "1"));
                                        bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                                        double x = bitmap.getWidth();
                                        double y = bitmap.getHeight();
                                        x = Math.floor(x / 2 - x * 0.3 / 2);
                                        y = Math.floor(y / 2 - x * 0.3 / 2);
                                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                                        System.out.println(x);
                                        System.out.println(y);
                                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                                        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                                        bitmap = Bitmap.createBitmap(bitmap, (int) x, (int) y, 500, 500);
                                        tessBaseAPI.setImage(bitmap);
                                        s = tessBaseAPI.getUTF8Text();
                                        System.out.println("-----------------------------------------------------------------------------------------");
                                        System.out.println("-----------------------------------------------------------------------------------------");
                                        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                                        System.out.println(s);
                                        textView.setText(s);
                                        tessBaseAPI.end();
                                    }
                                });
                            }
                        };
                        photoBtn.setOnClickListener(onClickListenerSudoku);
                }
            }
        };
        colorBut.setOnClickListener(onClickListenerColor);
        sudokuBut.setOnClickListener(onClickListenerColor);
    }


    public static String getColor(Bitmap bm) {

        HashMap<Integer, Integer> colorsPool = new HashMap<>();
        int imgX = (bm.getWidth() / 2) - 10;
        int imgY = (bm.getHeight() / 2) - 10;

        //       put one picsel to the pool
        int color = bm.getPixel(imgX - 1, imgY);

        colorsPool.put(color, 1);

        for (int y = imgY; y < imgY + 20; y++) {
            for (int x = imgX; x < imgX + 20; x++) {
                color = bm.getPixel(x, y);

                if (colorsPool.containsKey(color)) {
                    colorsPool.put(color, colorsPool.get(color) + 1);
                } else {
                    colorsPool.put(color, 1);
                }
            }
        }

        for (Integer key : colorsPool.keySet()) {

            int val = colorsPool.get(key);

            if (colorsPool.get(color) < val) {
                color = key;
            }
        }
        alpha = (color >> 24) & 0xff;
        red = (color & 0xff0000) >> 16;
        green = (color & 0xff00) >> 8;
        blue = color & 0xff;

        return new String(alpha + " " + red + " " + green + " " + blue);
    }
}
